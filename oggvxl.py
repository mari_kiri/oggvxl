"""
oggvxl, a tool to polyglot .ogg and .vxl files together
(One function was reverse-engineered from libogg, so there's licensing information for that)
Copyright (c) 2014 mari_kiri

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import sys, struct
import zlib
import random

# Set this to true to test the OggFrame class.
SKIP_VXL = False

# Set this to true to test... not splitting the Vorbis stream.
NO_SPLIT = False

VBSN = random.randint(0, 0xFFFFFFFF)
#horse = []

vorbpage = 0

"""
This function is derived from code from libogg because the relevant documentation is shit.

Copyright (c) 2002, Xiph.org Foundation

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

- Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

- Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

- Neither the name of the Xiph.org Foundation nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
def getcrc(s):
	#POLY = 0xEDB88320
	POLY = 0x04C11DB7
	p = 0x00000000

	for c in s:
		v = ord(c)
		for i in xrange(8):
			if v & 0x80: p ^= 0x80000000
			v = (v<<1) & 0xFF

			if p & 0x80000000: p = (p<<1) ^ POLY
			else: p <<= 1
			p &= 0xFFFFFFFF

	#return p ^ ((zlib.crc32(s, -1) ^ -1) & 0xFFFFFFFF)
	#return ((zlib.crc32(s, -1) ^ -1) & 0xFFFFFFFF)
	return p

class OggFrame:
	def __init__(self, src=None):
		if type(src) == type(""):
			self._load_str(src)
		elif src.__class__ == self.__class__:
			self._load_oggframe(src)
		elif src == None:
			self._load_defaults()
		else:
			self._load_fp(src)
	
	def recalc_len(self):
		self.segment_table = []
		size_left = len(self.data)
		while size_left >= 255:
			self.segment_table.append(size_left)
			size_left -= 255
		self.segment_table.append(size_left)
	
	def get_len(self):
		if self.segment_table == None:
			self.recalc_len()
			
		assert sum(self.segment_table) == len(self.data)

		return 27 + len(self.segment_table) + len(self.data)
	
	def get_data(self):
		self.get_len()

		ret = ""
		ret += "OggS"
		ret += struct.pack("<BB", self.version, self.header_type)
		ret += struct.pack("<I", self.granule_position & 0xFFFFFFFF)
		ret += struct.pack("<I", (self.granule_position>>32) & 0xFFFFFFFF)
		ret += struct.pack("<I", self.bitstream_serial_number)
		ret += struct.pack("<I", self.page_sequence_number)
		ret += struct.pack("<I", 0) # CRC-32 gets dropped into here
		ret += struct.pack("<B", len(self.segment_table)) 
		ret += "".join(chr(v) for v in self.segment_table)
		ret += self.data

		# Calculate CRC-32
		crc = getcrc(ret)
		#global horse
		#horse.append((len(ret), hex(crc), hex(self.crc_checksum), hex(crc ^ self.crc_checksum)))
		#print horse[-1]
		ret = ret[:22] + struct.pack("<I", crc) + ret[26:]
		
		return ret
	
	def split_for_room(self, room):
		# If we can fit the whole packet into the room,
		# we return the data and None.
		if NO_SPLIT or room >= self.get_len():
			return self.get_data(), None

		# If we can't fit the start of this packet into the gap,
		# we return an empty string and this very packet.
		if room < 27 + 1 + self.segment_table[0]:
			return "", self

		# Otherwise, we progress through the segment table.
		accl = 27
		segs = 0
		while True:
			naccl = accl + 1 + self.segment_table[segs]

			# If there is enough room, continue.
			if naccl <= room:
				accl = naccl
				segs += 1
				continue

			# Otherwise go with the number of segments we can offer.
			dsplit = sum(self.segment_table[:segs])
			p = OggFrame(self)
			n = OggFrame(self)
			p.segment_table = self.segment_table[:segs]
			n.segment_table = self.segment_table[segs:]
			p.data = self.data[:dsplit]
			n.data = self.data[dsplit:]

			p.header_type &= ~0x04
			#n.header_type |=  0x01

			# TODO: sort out the page sequence number stuff

			d = p.get_data()
			assert len(d) == accl
			assert len(d) <= room
			#print "!", len(d), room
			return d, n

	def _load_oggframe(self, o):
		self.version = o.version
		self.header_type = o.header_type
		self.granule_position = o.granule_position
		self.bitstream_serial_number = o.bitstream_serial_number
		self.page_sequence_number = o.page_sequence_number
		self.crc_checksum = o.crc_checksum
		self.page_segments = o.page_segments
		self.segment_table = o.segment_table
		self.data = o.data
	
	def _load_defaults(self):
		self.version = 0
		self.header_type = 0
		self.granule_position = (-1) & ((1<<64)-1)
		self.bitstream_serial_number = None
		self.page_sequence_number = None
		self.crc_checksum = 0
		self.page_segments = 0
		self.segment_table = None
		self.data = ""
	
	def _load_str(self, s):
		raise Exception("TODO")

	def _load_fp(self, fp):
		magic = fp.read(4)
		if magic != "OggS":
			raise Exception("Ogg frame missing. An Ogg file is NOT an MP3 file!")
		self.version, self.header_type, granule_position_low, = struct.unpack("<BBI", fp.read(6))
		granule_position_high, = struct.unpack("<I", fp.read(4))
		self.granule_position = (granule_position_high<<32) | granule_position_low
		self.bitstream_serial_number, = struct.unpack("<I", fp.read(4))
		self.page_sequence_number, = struct.unpack("<I", fp.read(4))
		self.crc_checksum, = struct.unpack("<I", fp.read(4))
		self.page_segments, = struct.unpack("<B", fp.read(1))
		self.segment_table = [ord(fp.read(1)) for i in xrange(self.page_segments)]
		self.data = fp.read(sum(self.segment_table))

print "Loading Ogg data"
oggfp = open(sys.argv[1], "rb")
ogg = []
frames_active = set()
frames_ended = set()
while len(frames_active) > 0 or len(frames_ended) == 0:
	f = OggFrame(oggfp)
	print f.header_type, f.bitstream_serial_number, f.page_sequence_number, f.granule_position, f.get_len(), f.segment_table

	if f.bitstream_serial_number in frames_ended:
		raise Exception("Stream %08X should not appear after it has ended" % f.bitstream_serial_number)
	if f.header_type & 0x02:
		if f.bitstream_serial_number in frames_active:
			raise Exception("Stream %08X should not start after it has already appeared" % f.bitstream_serial_number)
		frames_active.add(f.bitstream_serial_number)
	if not f.bitstream_serial_number in frames_active:
		raise Exception("Stream %08X should not appear after it has ended" % f.bitstream_serial_number)
	if f.header_type & 0x04:
		if f.bitstream_serial_number not in frames_active:
			raise Exception("Stream %08X should not end before it has started" % f.bitstream_serial_number)
		frames_ended.add(f.bitstream_serial_number)
		frames_active.remove(f.bitstream_serial_number)

	ogg.append(f)

oggfp.close()

maph = 0

def read_vxl_column(fp):
	global maph
	r = ""
	while True:
		r += fp.read(4)
		d,s,e,a = struct.unpack("<BBBB", r[-4:])

		if e+1 >= maph:
			maph = e+1

		if d == 0:
			r += fp.read(4*max(0, (e-s+1)))
			break
		else:
			r += fp.read(4*(d-1))
	
	return s

vxlfp = open(sys.argv[2], "rb")
print "Loading map"
vxl = [[read_vxl_column(vxlfp) for x in xrange(512)] for y in xrange(512)]
vxlfp.seek(0)
vxl = vxlfp.read()
vxlfp.close()

print "Map height:", maph
print "Validating map integrity"

# TODO: other tests
if maph < (ord("O")-1) + ord("g"):
	print "Map too low, needs raising"
	print "TODO: raise it. We're going to have issues anyway."

print "Polyglotting!"

hell = open(sys.argv[3], "wb")
print "Constructing Ogg stream"

# We have 316 bytes to play with initially.
vfpsn = 0
def vfmake(data, flg=0):
	global vfpsn

	vf = OggFrame()
	vf.granule_position = 0
	vf.page_sequence_number = vfpsn
	vfpsn += 1
	vf.bitstream_serial_number = VBSN
	vf.data = data
	vf.header_type = flg

	return vf

vfinit = vfmake("vxlhack\x00" + "\x00"*8, flg=0x02)

#gap_len = 316 - len(d)
gap_len = 316 - len(vfinit.get_data())

# Because greedy algorithms are totally optimal.
for o in ogg:
	while True:
		o.page_sequence_number = vorbpage
		vorbpage += 1
		# Split the packet if possible.
		d, o = o.split_for_room(gap_len - 28)
		hell.write(d)
		gap_len -= len(d)

		if vfinit != None:
			if not SKIP_VXL:
				hell.write(vfinit.get_data())
			vfinit = None

		# If we have sent the whole packet, we move onto the next one.
		# No vxlhack packet necessary.
		if o == None:
			break

		# Add a vxlhack packet.
		data = "\x00" * (gap_len - 28) + chr(maph+1) + "\x00" + chr(maph-1) + "\x00"
		assert len(data) < 510
		vf = vfmake(data)
		#print len(data)
		if len(vf.data) < 255*1:
			vf.segment_table = [len(vf.data)]
		elif len(vf.data) < 255*2:
			vf.data = vf.data[1:]
			vf.segment_table = [254, len(vf.data) - 254]

		data = vf.get_data()
		assert gap_len - len(data) == -4

		if not SKIP_VXL:
			hell.write(data)
		gap_len = maph*4

if not SKIP_VXL:
	# Add a terminating vxlhack packet.
	vf = vfmake("", 0x04)
	d = vf.get_data()
	hell.write(d)
	gap_len -= len(d)
	assert gap_len >= 0

	# Pad the remainder.
	hell.write("\x00"*gap_len)

	# Dump the map and quit.
	hell.write(vxl)

hell.close()

#horse.sort()
#for l in horse:
#	print l



